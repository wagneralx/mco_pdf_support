/*
 * Movilitas FormViewer Component Script
 * Build for: 	Erdgas Mittelsachsen [EMS]
 * Type:		GA10 - Sichtkontroller BioGas
 *				nach DVGW-Arbeitsblatt G495
 * Status: 		Approved
 * Version:		1, Nov. 2018
 *
 */

var main = (function() {

	var counter = 0;
	var documentIsValid = true;

	// mandatory radiobutton fields
	var fields = [
		"Gebaeude_Allgemein",
		"Gebaeude_Heizung",
		"Gebaeude_Klimaanlage",
		"Gebaeude_Gaswarnanlage",
		"Gebaeude_Brandmeldeanlage",
		"Messanlage_Druckmessstellen",
		"Messanlage_Temperaturmessstellen",
		"Messanlage_HDStaubfilter",
		"Messanlage_Drehkolbengaszaehler",
		"Messanlage_PGC",
		"Messanlage_H2S",
		"Messanlage_SAV",
		"Messanlage_Armaturen",
		"Messanlage_Leitungen",
		"Messanlage_Ringwellschlauch",
		"Messanlage_Flaschenbatterien",
		"Konditionierungsanlage_Umfang",
		"Verdichteranlage_Allgemein",
		"EMSR_Gesamtanlage",
		"Druckluft_Gesamtanlage",
		"Odoranlage_Gesamtanlage",
		// // //
		"Status_BGEA",
		"Status_Verdichter1",
		"Status_Verdichter2",
		"Status_Brandmeldeanlage",
		"Status_Gaswarnanlage",
		"Status_PGC1",
		"Status_H2S",
		"Status_MRGEin",
		"Status_MRGAus",
		"Status_Konditionierung",
		"Status_ProLuft",
		"Status_Odor",
		"Status_Klimaanlage",
		"Odorierung_Funktionspruefung"
		];

	// mandatory text fields
	var fields_txt = [
		"Druck_Anlageneingang_Txt",
		"Druck_Anlagenausgang_Txt",
		"Druck_Steuerluft_Txt",
		"Temperatur_Anlageneingang_Txt",
		"Temperatur_Anlagenausgang_Txt",
		"Temperatur_Taupunkt_Txt",
		"Raumtemp_MessraumIst_Txt",
		"Raumtemp_MessraumLuftkonzentration_Txt",
		"Raumtemp_ERaumIst_Txt",
		"Raumtemp_OdorraumIst_Txt",
		"Raumtemp_OdorraumLuftkonzentration_Txt",
		// // //
		"Messung_EingangDKZVb_Txt",
		"Messung_EingangVb_Txt",
		"Messung_EingangVb2_Txt",
		"Messung_AusgangMRGVn_Txt",
		"Foerdermenge_EingangMRGVn_Txt",
		"Foerdermenge_AusgangMRGVn_Txt",
		"LPGBehaelter_PLS_Txt",
		"OdorBehaelter_Ltr_Txt",
		"Prueffrist_Feuerloescher_Txt",
		// // //
		"PGC_BrennwertEin_Txt",
		"PGC_BrennwertAus_Txt",
		"PGC_MRGEin_Txt",
		"PGC_MRGAus_Txt",
		"PGC_CH4Ein_Txt",
		"PGC_CH4Aus_Txt",
		"PGC_O2Ein_Txt",
		"PGC_O2Aus_Txt",
		"PGC_CO2Ein_Txt",
		"PGC_CO2Aus_Txt",
		"PGC_N2Ein_Txt",
		"PGC_N2Aus_Txt",
		"PGC_C3H8Aus_Txt",
		// // //
		"PGC_Kalibriergas_Txt",
		"PGC_ArgonTraeger1_Txt",
		"PGC_ArgonTraeger2_Txt",
		"PGC_VD1_Txt",
		"PGC_VD2_Txt",
	];

	var endsWith = (function(str, suffix) {
	    return str.indexOf(suffix, str.length - suffix.length) !== -1;
	});

	var fieldExists = (function(fieldname) {
		var fe = false;
		if (this.getField(fieldname) != null) {
			fe = true;
		}
		return fe;
	});

	// check if any defects are visible
	var checkDefects = (function() {

		// debug: clear textbox
		// this.getField("errorHandler").value = "";

		fields.forEach(function(fieldname) {
			var maengel = false;
			var fieldname_maengel = "";

			// define maengel and chec if assigned text fields are available
			// probably ths <string>_Maengel is not null, because of contained chars
			if( this.getField(fieldname.toString()+"_Maengel") != null ) {
				fieldname_maengel = fieldname.toString()+"_Maengel";
				maengel = true;
			}

			if (this.getField(fieldname) != null) {
				// radiobutton is "iO"
				if (endsWith(this.getField(fieldname).value, "_iO")) {
					this.getField(fieldname_maengel).readonly = true;
					this.getField(fieldname_maengel).borderColor = "black";
					counter +=1;
				} else {
					// radiobutton is "niO"
					if (endsWith(this.getField(fieldname).value, "_niO")) {
						if (this.getField(fieldname_maengel).value == "") {
							this.getField(fieldname_maengel).readonly = false;
							//this.getField(fieldname_maengel).borderColor = "red";
							counter += 1000;
							// this.getField("errorHandler").value += "***"+fieldname_maengel;
						} else {
							//this.getField(fieldname_maengel).borderColor = "green";
							counter +=1;
						}
					} else {
						counter +=1000;
					}
				}
			} else {
				// console.log("field " + this.getField(fieldname) + " not found");
				counter += 1000;
				// debug: show field in textbox
				// this.getField("errorHandler").value += fieldname.toString();
			}
		});
/*
		if (counter >= 1000) {
			this.getField("Maengel").value = "Maengel_j";
		} else {
			this.getField("Maengel").value = "Maengel_n";
		}
*/

		// this.getField("counter").value = counter;

	});

	// check if mandatory fields are filled completely
	var checkMandatoryFields = (function() {

		for (index = 0; index < fields.length; index++) {
		    var field = this.getField(fields[index]);
			// if(field != null) {
		    	documentIsValid = documentIsValid  && (endsWith(field.value, "iO"));
				if (documentIsValid == true) {
					// this.getField("errorHandler").value += field.value + "\r\n";
				}
			// }
		}

		// debug: status of radiobuttons
		// this.getField("errorHandler").value += documentIsValid.toString() + "\r\n";

		for (index = 0; index < fields_txt.length; index++) {
		    var field = this.getField(fields_txt[index]);
			// if(field != null) {
		    	documentIsValid = documentIsValid  && (field.value != "");
				if (documentIsValid == true) {
					// this.getField("errorHandler").value += field.value + "\r\n";
				}
			// }
		}

		// debug
		// this.getField("errorHandler").value += documentIsValid.toString() + "\r\n";

		// if documentIsValid == true, set hidden radiobutton to "_Ja"
		if (documentIsValid) {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
		} else {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
		}

	});

	checkDefects();
	checkMandatoryFields();

});

main();
