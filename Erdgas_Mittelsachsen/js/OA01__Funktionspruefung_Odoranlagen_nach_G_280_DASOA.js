/*
 * Movilitas FormViewer Component Script
 * Build for: 	Erdgas Mittelsachsen [EMS]
 * Type:		OA01 - Monatliche Funktionsprüfung Odoranlage
 *				nach DVGW-Arbeitsblatt G280
 * Status: 		Approved
 * Version:		1, Nov. 2018
 *
 */

var main = (function() {

	// Error Handling
	var errMsg = [];

	// Mandatory Fields
	var fields = [
			"Einstellwert",
			"Odorvolumen",
			"Gasvolumen",
			"errechneteOdormenge",
			"Fuellstand"
		];

	var defects = [];				// Defects
	var documentIsValid = true;		// Mandatory fields complete
	var odorVol = null;				// Odorvolume
	var gasVol = null;				// Gasvolume
	var calc_odorVol = 0;			// calculated Odor amount

	// calculate Odor amount
	var calculateOdor = (function () {

		if( this.getField("Odorvolumen").value != null) {
		    odorVol = this.getField("Odorvolumen").value;
		}

		if( this.getField("Gasvolumen").value != null) {
		    gasVol = this.getField("Gasvolumen").value;
		}

		if( (odorVol != null) && (gasVol > 0) ) {
		    calc_odorVol = (odorVol * 1000 * 60 / gasVol).toFixed();
		}

		var txt_cOdorVol = this.getField("errechneteOdormenge");
		txt_cOdorVol.value = calc_odorVol;
	});

	// check if any defects are visible
	var checkDefects = (function() {

		var cb_maengel = this.getField("Maengel");

		if (cb_maengel.value == "Maengel_j") {
			// Erlaueterung zum Mangel ist Pflicht
			this.getField("Fehler").value = "Bitte geben Sie weitere Einzelheiten zu den Mängeln an.";
			this.getField("ErlaeterungMaengel").readonly = false;
			if ( (this.getField("ErlaeterungMaengel").value) == "") {
				documentIsValid = false;
			} else {
				documentIsValid = true;
			}
		} else {
			this.getField("Fehler").value = "";
			this.getField("ErlaeterungMaengel").readonly = true;
			this.getField("ErlaeterungMaengel").value = "";
		}
	});

	// check if mandatory fields are filled completely
	var checkMandatoryFields = (function() {

		fields.forEach(function(fieldname) {
			if ( this.getField(fieldname).value == null || this.getField(fieldname).value == "")  {
				documentIsValid = false;
				// app.alert(fieldname, 3);
			}
		});

		// if documentIsValid == true, set hidden radiobutton to "_Ja"
		if (documentIsValid) {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
		} else {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
		}

	});

	// Array for possible error handling
	var addError = (function() {
		var txtErrorMsg = this.getField("Fehler");
		errMsg.forEach(function(entry) {
		   txtErrorMsg.value = entry + "\r\n";
		});

	});

	// call functions
	calculateOdor();
	checkDefects();
	checkMandatoryFields();

});

main();
