/*
 * Movilitas FormViewer Component (c) 2018
 * Build for: 	Erdgas Mittelsachsen [EMS]
 * Type:		Durchleitungsdruckbehälter - Äußere Prüfung
 *				nach DVGW-Arbeitsblatt G498
 * Status: 		Approved
 * Version:		1, Nov. 2018
 *
 */

 var main = (function() {

	var counter = 0;
 	var documentIsValid = true;

	// group definition for autocompleting checkboxes per group
	var gasseitigesSBV = [ "Gasseitiges_SBV", "_Typ", "_FaNr", "_bar" ];
	var wasserseitigesSBV = [ "Wasserseitiges_SBV", "_Typ", "_FaNr", "_bar" ];
	var wasserseitigesSAV1 = [ "1_Wasserseitiges_SAV", "_Typ", "_FaNr", "_bar" ];
	var wasserseitigesSAV2 = [ "2_Wasserseitiges_SAV", "_Typ", "_FaNr", "_bar" ];
	var tempbegrenzer = [ "Temperaturbegrenzer", "_Typ", "_FaNr", "_T" ];

	// which groups will be autocompleted
	var pruefungen = [
		gasseitigesSBV,
		wasserseitigesSBV,
		wasserseitigesSAV1,
		wasserseitigesSAV2,
		tempbegrenzer
	];

	// autocomplete per group
	var fieldGroupComplete = (function(subfields, prefix) {
		var group = [false, false, false];
		var field = subfields[0];

		for (index = 1; index < subfields.length; index++) {
			var f = this.getField(prefix+field+subfields[index]);
			// app.alert(prefix+field+subfields[index]);
			if (f.value == "") {
		    	group[index-1] = 0;
			} else {
				group[index-1] = 1;
			}

			if ( ( group[0] && group[1] && group[2] ) == true) {
				this.getField(field).value = field+"_iO";
			} else {
				// if not manually set ti niO
				if(this.getField(field).value != field+"_niO") {
					// hidden
					this.getField(field).value = field+"_nV_iO";
				}
			}
		}
	});

	// select all fields with the following prefix
	var prefix = "Pruefung_";
	pruefungen.forEach(function(item) {
		fieldGroupComplete(item, prefix);
	});

	// radiobutton arrray
	var buttons = [
		"Siko_DLBehaelter",
		"Siko_Ausruestung",
		"Siko_Verschluss",
		"Differenzdruck",
		"Druckentlastung",
		"Differenzdruck",
		"Druckentlastung",
		"Erdbedeckte_Kondensatabscheider",
		];

	// textfield array
	var textfields = [
		"Fabriknummer",
		"Behaelterart",
		"Typ",
		"Baujahr",
		"Lieferant",
		"Medium",
		"PS_00",
		"PS_01",
		"PS_10",
		"PS_11",
		"Nennweite",
		"Inhalt",
		"Kategorie",
		"Min_Temp_0",
		"Min_Temp_1",
		"MIP",
		"Datum_Erstmalig",
		"Datum_Letzte_Aeussere",
		"Datum_Letzte_Innere",
		"Datum_Letzte_Festigkeit",
		"Pruefergebnisse",
		"Datum_Erstmalig",
		"Datum_Naechste_Aeussere",
		"Datum_Naechste_Innere",
		"Datum_Naechste_Festigkeit",
		"Gasseitiges_SBV",
		"Wasserseitiges_SBV",
		"1_Wasserseitiges_SAV",
		"2_Wasserseitiges_SAV",
		"Temperaturbegrenzer",
		"Datum_Funktionspruefung_verschoben",
		];

	var endsWith = (function(str, suffix) {
	    return str.indexOf(suffix, str.length - suffix.length) !== -1;
	});

	var setFieldVal = (function(target, source) {
		if( (this.getField(target) != null) && (this.getField(source) != null) ) {
			this.getField(target).value = this.getField(source).value;
		}
	});

	var checkDefects = (function() {

		// debug: clear textbox
		// this.getField("ErlaeuterungMangel").value = "";

		// check textfield for values, increase counter and add name to ErlaeuterungMangel"
		textfields.forEach(function(fieldname) {
			// if exists
			if(this.getField(fieldname) != null) {
				var f = this.getField(fieldname).value;
				if ((f != "") || (endsWith(f, "_iO"))) {
					counter += 1;
				} else {
					counter += 100;
					// this.getField("ErlaeuterungMangel").value += fieldname;
				}
			}
		});

		// check radiotbuttons for values, increase counter and add name to ErlaeuterungMangel"
		buttons.forEach(function(buttonName) {
			// if exists
			if(this.getField(buttonName) != null) {
				var b = this.getField(buttonName).value;
				if ((endsWith(b, "_iO"))) {
					counter += 1;
				} else {
					counter += 100;
					// this.getField("ErlaeuterungMangel").value += buttonName;
				}
			}
		});

		// this.getField("ErlaeuterungMangel").value += "\r\n Counter: " + counter;

		/*
		// if unfilled fields left
		if (counter >= 100) {
			this.getField("Maengel").value = "Maengel_j";
		}
		// if all filled
		if(counter < 100){
			this.getField("Maengel").value = "Maengel_n";
		}
		*/

		// this.getField("ErlaeuterungMangel").value += "\r\n Mangel: " + this.getField("Maengel").value;

	});

	var checkMandatoryFields = (function() {
		// if radiobutton 2 or 3 on page 1 is set, field "Anmerkungen" on page 3 must not be ""

		// if all textfield filled
		textfields.forEach(function(fields) {
			var field = this.getField(fields).value;
			 if (field == "") {
		    	documentIsValid = false;
			 }

			 // "Anmerkungen" is mandatory, if set like mentionend above (radiobuttons p.1)
			 var pruefergebnisse = this.getField("Pruefergebnisse").value;
			 var anmerkungen = this.getField("ErlaeuterungMangel").value;
			 if( 	(endsWith(pruefergebnisse, "_mA_niO") && (anmerkungen == "") ) ||
			 		(endsWith(pruefergebnisse, "niO") && (anmerkungen== "") ) )
				{
					this.getField("ErlaeuterungMangel").borderColor = "red";
					documentIsValid = false;
			 	}
		});

		// if alle buttons are checked
		buttons.forEach(function(fields) {
			var field = this.getField(fields).value;
			 if (!(endsWith(field, "iO"))) {
		    	documentIsValid = false;
			 }
		});

		// debug: show status
		//this.getField("ErlaeuterungMangel").value += "\r\n Status: " + documentIsValid;

		// if documentIsValid == true, set hidden field
		if (documentIsValid) {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
		} else {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
		}
	});

	setFieldVal("FabriknrS2", "Fabriknummer");
	setFieldVal("FabriknrS3", "Fabriknummer");

	checkDefects();
	checkMandatoryFields();

});

main();
