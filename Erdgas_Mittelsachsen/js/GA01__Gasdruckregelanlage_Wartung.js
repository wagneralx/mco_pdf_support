/*
 * Movilitas FormViewer Component Script
 * Build for: 	Erdgas Mittelsachsen [EMS]
 * Type:		GA01 - Wartungsprotokoll Gasdruckregelanlage
 *				nach DVGW-Arbeitsblatt G495
 * Status: 		Not finished
 * Version:		2, Feb. 2019
 *
 */

var main = (function() {

	var counter = 0;
	var documentIsValid = true;

	var fields = [
		"Art_der_Baumaßnahme",
		"Art_der_Anlage",
		"Zusaetzliche_Maßnahmen_Freigabe_Aufnahme_Arbeiten",
		"Aeusserer_Zustand",
	 	"Abdichtungen_zu_Nebenraeumen",
	 	"Freier_Zugang",
	 	"Dichtheit_Bauelemente",
	 	"Anlagenschema",
	 	"Korrosion_Anstrich_Baugruppen",
	 	"Sauberkeit_Fussboden",
	 	"Be_und_Entlueftungsklappen",
	 	"Freie_Strassenkappe",
	 	"Gaengigkeit_Stellung",
	 	"Beschilderung",
	 	"Dichtigkeit",
	 	"Staub_Kondensatanfall",
	 	"Staubbelastung",
	 	"Dichtigkeit_Ausruestungsteile",
	 	"Dichter_Abschluss_SAV",
	 	"Ausblaeserkondensat",
	 	"Beatmungsventile",
	 	"Stellung_und_Stellungsanzeige",
	 	"Leckgaszaehler",
	 	"Dichter_Abschluss_SBV",
	 	"Arbeitsweise",
	 	"Dichter_Abschluss_DRG",
	 	"Absperr_Innere_Dichtigkeit",
	 	"Absperr_Gaengigkeit",
	 	"Absperr_Stellung",
	 	"Rohrleitungen_Beschaedigung",
	 	"Rohrleitungen_Dichtheit",
	 	"Rohrleitungen_Halterungen",
	 	"Rohrleitungen_Ausblase"
	 	];

	// array: show / hide fields of paragraph "Gebäude und Außenanlage"
	var fieldset1 = [
		"Aeusserer_Zustand",
		"Abdichtungen_zu_Nebenraeumen",
		"Freier_Zugang",
		"Dichtheit_Bauelemente",
		"Korrosion_Anstrich_Baugruppen",
		"Anlagenschema",
		"Sauberkeit_Fussboden",
		"Be_und_Entlueftungsklappen",
		"Raumatmoshpäre"
	];

	// array: show / hide fields of paragraph "Erdverlegte Absperreinrichtung"
	var fieldset2 = [
		"Freie_Strassenkappe",
		"Gaengigkeit_Stellung",
		"Beschilderung",
		"Dichtigkeit"
	];

	// array: show / hide fields of all paragraphs, exception for textfields (no checkbox -> "iO")
	var txt_exceptions = [
		"Raumatmoshpäre"
	];

	var endsWith = (function(str, suffix) {
	    return str.indexOf(suffix, str.length - suffix.length) !== -1;
	});

	// function: show / hide paragraph
	var switchParagraph = (function(fieldset, exc, state) {
		// get array of fields and set checkboxes as readonly, as well as _iO
		fieldset.forEach(function(fieldname) {
			if(state == "inactive") {
				this.getField(fieldname).readonly = true;
				// quick fix: exclude dropdownlist by fieldname "Auswahl_*"
				var prefix = "Auswahl";
				if(fieldname.indexOf(prefix) == -1) {
					this.getField(fieldname).value = fieldname+"_nV_iO";
				}
				// if txt field in exception list overwrite field value
				if(exc.indexOf(fieldname) != -1) {
					if (fieldname.indexOf(prefix) != -1) {
						this.getField(fieldname).readonly = true;
					} else {
						this.getField(fieldname).value = "n.A.";
					}
				}
			}
			if(state == "active") {
				this.getField(fieldname).readonly = false;
			}
		});
	});

	var toggleFormFields = (function(cb_value, txt_exceptions, fieldset) {

		if(this.getField(cb_value).value == "Ja") {
			switchParagraph(fieldset, txt_exceptions, "inactive");
		} else {
			switchParagraph(fieldset, [], "active");
		}
	});

	// check if any defects are visible
	var checkDefects = (function() {

		// debug: clear textbox
		// this.getField("ErlaeterungMangel").value = "";

		fields.forEach(function(fieldname) {
			if (endsWith(this.getField(fieldname).value, "_iO")) {
				counter +=1;
			} else {
				counter += 100;
				// debug: show field in textbox
				// this.getField("ErlaeterungMangel").value += fieldname;
			}
		});

		if (counter >= 100) {
			this.getField("Maengel").value = "Maengel_j";
		} else {
			this.getField("Maengel").value = "Maengel_n";
		}

	});

	// check if mandatory fields are filled completely
	var checkMandatoryFields = (function() {

		for (index = 0; index < fields.length; index++) {
		    var field = this.getField(fields[index]);
		    documentIsValid = documentIsValid  && (endsWith(field.value, "iO") || field.value == "On" || field.value == "Ja");
		}

		// if documentIsValid == true, set hidden radiobutton to "_Ja"
		if (documentIsValid) {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
		} else {
		    this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
		}

	});

	toggleFormFields("nV_GA", txt_exceptions, fieldset1);
	toggleFormFields("nV_EA", [], fieldset2);

	checkDefects();
	checkMandatoryFields();

});

main();
