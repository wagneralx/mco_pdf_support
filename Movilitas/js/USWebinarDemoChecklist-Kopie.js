function main(){

	// Array der Radio-Buttons
	var checkboxArray = [
						"IsolationControll_iO",
						"IsolationControll_niO-err-notif",
						"SolidityControll_iO",
						"SolidityControll_niO-err-notif",
						"AnchoringControll_iO",
						"AnchoringControll_niO-err-notif",
						"NoiseControll_iO",
						"NoiseControll_niO-err-notif"
						];

	var textfieldArray = [
						"PressureText",
						"MeterValueText"
						];

	var documentIsValid = true;
	var counterButtons = 0;
	var counterTextFields = 0;

	// Ueberpruefung der Radio-Buttons
	checkboxArray.forEach(function(checkBoxName){
		if(this.getField(checkBoxName).value != ""){
			counterButtons++;
		}
	});

	// Ueberpruefung der Textfelder
	textfieldArray.forEach(function(textfieldName){
		if(this.getField(textfieldName).value != ""){
			counterTextFields++;
		}
	});

	if(counterButtons < 4 || counterTextFields < 2){
		documentIsValid = false;
	}

	if(documentIsValid){
		this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
	}else{
		this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
	}

}

main();
