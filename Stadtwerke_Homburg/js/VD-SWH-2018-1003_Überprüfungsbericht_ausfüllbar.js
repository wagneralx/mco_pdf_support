/*
 * Hier werden die Checkboxen im Maengelbereich auf ihren Inhalt geprueft.
 * Wenn mindestens eine der Checkboxen gefuellt ist, muss auch
 * mindestens eines der Textfelder, aus dem selben Bereich, ausgefuellt sein.
 * Sonst wird ein Fehler ausgeloest und das Dokument kann nicht validiert werden.
 */
function main(){

	console.log("Debug Ausgabe");


	// Array von Checkboxen (Maengel)
	var maengelCheckBox = [
			"Maengel_KorrosionAnInstallation",
			"Maengel_AnlageMussGesichertWerden",
			"Maengel_HAEnichtVorhanden",
			"Maengel_RohrkapselOderFutterrohrFehlen",
			"Maengel_HAEnichtGueltig",
			"Maengel_HAEistNichtZugaenglich",
			"Maengel_HAEmitOffenemBoden",
			"Maengel_GaszaehlerplaketteNichtVorhanden",
			"Maengel_Sonstige"
			];
	//Array von Textfeldern (Maengel)
	var maengelTextField = [
			"ErlaeuterungMaengel1"
			];
	// Variable zur Gueltigkeitsueberpruefung des Dokuments
	var documentIsValid = true;

	// Maengelfelder instanziieren
	var korrosionAnInstallation = null;
	var anlageMussGesichertWerden = null;
	var hAEnichtVorhanden = null;
	var rohrkapselOderFutterrohrFehlen = null;
	var hAEnichtGueltig = null;
	var hEistNichtZugaenglich = null;
	var hAEmitOffenemBoden = null;
	var gaszaehlerplaketteNichtVorhanden = null;
	var sonstige = null;

	// Maengelfelder initialisieren
	korrosionAnInstallation = this.getField("Maengel_KorrosionAnInstallation").value;
	console.log(korrosionAnInstallation);
	anlageMussGesichertWerden = this.getField("Maengel_AnlageMussGesichertWerden").value;
	hAEnichtVorhanden = this.getField("Maengel_HAEnichtVorhanden").value;
	rohrkapselOderFutterrohrFehlen = this.getField("Maengel_RohrkapselOderFutterrohrFehlen").value;
	hAEnichtGueltig = this.getField("Maengel_HAEnichtGueltig").value;
	hEistNichtZugaenglich = this.getField("Maengel_HAEistNichtZugaenglich").value;
	hAEmitOffenemBoden = this.getField("Maengel_HAEmitOffenemBoden").value;
	gaszaehlerplaketteNichtVorhanden = this.getField("Maengel_GaszÃ¤hlerplatteNichtVorhanden").value;
	sonstige = this.getField("Maengel_Sonstige").value;


	// Wenn eines der Maengelfelder ausgewaehlt ist...
	maengelCheckBox.forEach(function(checkBox){		// Schleife ueber das Array der Checkboxen
		if(this.getField(checkBox).value != null){
			//...und nicht mindestens eins der dazugehoerigen Textfelder ausgefuellt ist,...
			if(this.getField("ErlaeuterungMaengel1").value == ""){
				// ...ist das Dokument ungueltig
				documentIsValid = false;
			}else{
				// Ansonsten, ist das Dokument gueltig
				documentIsValid = true;
			}
		}
		if(documentIsValid){
			this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
		}else{
			this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
		}
	});
}

main();
