function main(){

	// Array aller vorhandener Radio-Buttons im Bereich Regler (Mitteldruckregler/ Zaehlerregler)
	var reglerRadioButtonArray = [
									"Mitteldruckregler_Reglereinbau",
									"Mitteldruckregler_Reglerpruefung",
									"Mitteldruckregler_Reglerwechsel_Turnuswechsel",
									"Mitteldruckregler_Reglerwechsel_Defekt",
									"Zaehlerregler_Zweirohr",
									"Zaehlerregler_Einrohr"
								];

	// Array aller Radio-Buttons fuer Nennweite im Bereich Technische Daten
	var technischeDatenNennweiteArray = [
									"Nennweite_25",
									"Nennweite_32",
									"Nennweite_40",
									"Nennweite_50"
								];

	// Array aller Radio-Buttons ja/nein im Bereich Technische Daten
	var techDatenJaNeinArray = [
									"Gesamtstroemungswaechter_iO",
									"Gesamtstroemungswaechter_niO",
									"Nullabschluss_iO",
									"Nullabschluss_niO",
									"Gasmangelsicherung_iO",
									"Gasmangelsicherung_niO",
									"Dichtungskontrolle_iO",
									"Dichtungskontrolle_niO",
									"Druckanstieg_iO",
									"Druckanstieg_niO"
								];

	var documentIsValid = true;
	var counterRegler = 0;
	var counterNennweite = 0;
	var counterJaNein = 0;

	// Instanziieren der Reglervariablen
	var reglereinbau = null;
	var reglerpruefung = null;
	var turnuswechsel = null;
	var defekt = null;
	var zweirohr = null;
	var einrohr = null;

	// Initialisieren der Reglervariablen
	reglereinbau = this.getField("Mitteldruckregler_Reglereinbau").value;
	reglerpruefung = this.getField("Mitteldruckregler_Reglerpruefung").value;
	turnuswechsel = this.getField("Mitteldruckregler_Reglerwechsel_Turnuswechsel").value;
	defekt = this.getField("Mitteldruckregler_Reglerwechsel_Defekt").value;
	zweirohr = this.getField("Zaehlerregler_Zweirohr").value;
	einrohr = this.getField("Zaehlerregler_Einrohr").value;

	// Instanziieren der Nennweitevariablen
	var nennweiteDN_25 = null;
	var nennweiteDN_32 = null;
	var nennweiteDN_40 = null;
	var nennweiteDN_50 = null;

	// Initialisieren der Nennweitevariablen
	nennweiteDN_25 = this.getField("Nennweite_25").value;
	nennweiteDN_32 = this.getField("Nennweite_32").value;
	nennweiteDN_40 = this.getField("Nennweite_40").value;
	nennweiteDN_50 = this.getField("Nennweite_50").value;

	// Instanziieren der JaNeinVariablen
	var gesamtstroemungswaechter_iO = null;
	var gesamtstroemungswaechter_niO = null;
	var nullabschluss_iO = null;
	var nullabschluss_niO = null;
	var gasmangelsicherung_iO = null;
	var gasmangelsicherung_niO = null;
	var dichtungskontrolle_iO = null;
	var dichtungskontrolle_niO = null;
	var druckanstieg_iO = null;
	var druckanstieg_niO = null;

	// Instanziieren der JaNeinVariablen
	gesamtstroemungswaechter_iO = this.getField("Gesamtstroemungswaechter_iO").value;
	gesamtstroemungswaechter_niO = this.getField("Gesamtstroemungswaechter_niO").value;
	nullabschluss_iO = this.getField("Nullabschluss_iO").value;
	nullabschluss_niO = this.getField("Nullabschluss_niO").value;
	gasmangelsicherung_iO = this.getField("Gasmangelsicherung_iO").value;
	gasmangelsicherung_niO = this.getField("Gasmangelsicherung_niO").value;
	dichtungskontrolle_iO = this.getField("Dichtungskontrolle_iO").value;
	dichtungskontrolle_niO = this.getField("Dichtungskontrolle_niO").value;
	druckanstieg_iO = this.getField("Druckanstieg_iO").value;
	druckanstieg_niO = this.getField("Druckanstieg_niO").value;

	// Ueberpruefung der Regler-Radio-Buttons
	reglerRadioButtonArray.forEach(function(checkBox){
		if(this.getField(checkBox).value != ""){
			counterRegler++;
		}
	});
	// Ueberpruefung der Nennweite-Radio-Buttons
	technischeDatenNennweiteArray.forEach(function(checkBox){
		if(this.getField(checkBox).value != ""){
			counterNennweite++;
		}
	});
	// Ueberpruefung der JaNein-Radio-Buttons
	for(index = 0; index < (techDatenJaNeinArray.length); index++){
		if((this.getField(techDatenJaNeinArray[index]).value != "") || (this.getField(techDatenJaNeinArray[index+1]).value != "")){
			counterJaNein++;
			index++;
		}
	}

	if(counterRegler == 0 || counterNennweite == 0 || counterJaNein == 0){
		documentIsValid = false;
	}

	if(documentIsValid){
		this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Ja";
	}else{
		this.getField("PflichtfelderAusgefuellt").value = "PflichtfelderAusgef_Nein";
	}
}

main();
